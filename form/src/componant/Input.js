import React from "react";
import '../App.css';



class Input extends React.Component {
  constructor(prors) {
    super(prors);
    this.state = {
      inputtext: " "
      ,changButton:false
    };
  }

  render() {
    return (
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-6">
            <input
              class="form-control"
              type="text"
              placeholder="name"
              onChange={e =>
                this.setState({
                  inputtext: e.target.value
                })
              }
              value={this.state.inputtext}
            />
          </div>
  
          <div class="col">
            <input type="submit" class="btn btn-secondary" value="ADD" onClick={() =>this.props.onClickAdd(this.state)} />
          </div>
        </div>
      </div>
    );
  }
}

export default Input;
