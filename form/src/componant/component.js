import React from "react";
import Input from "../componant/Input";
import Display from "../componant/Display";

import '../App.css';



class AppComponent extends React.Component {
    constructor(prors) {
        super(prors);
        this.state = {
            dataall: []

        };
    }

    callData = (data) => {
        var dataallNaja = this.state.dataall
        dataallNaja.push(data)
        this.setState({ dataall: dataallNaja });


    };
    onChangebutton = (index) => {
        
        
        let data = this.state.dataall
       
         data[index].changButton =true

        this.setState({
            dataall: data
        })


 };
 onClickRemove=(index)=>{

        let data = this.state.dataall
        data.splice(index, 1)
        this.setState({
            dataall: data
        })

    

 }


    
   
    callClearn = () => {
        this.setState({
            dataall: [],

        })
    }

    render() {
        return (
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-6">
                        <Input onClickAdd={this.callData} />
                        <Display sentData={this.state.dataall} chang={this.onChangebutton} changbutton={this.onClickRemove} />

                    </div>
                </div>
            </div>
        );
    }
}

export default AppComponent;
