import React from 'react';
import { render } from 'react-dom';
import '../App.css';
const Display = (props) => {



    return (
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Todo</th>
                </tr>
            </thead>
            <tbody>
                {
                    props.sentData.length > 0 ?
                        props.sentData.map((data, index) => {
                            return (
                                <tr>
                                    <th scope="row">{index + 1}</th>
                                    {data.changButton == false ?
                                        <td>{data.inputtext}</td>
                                        :
                                        <td><s>{data.inputtext}</s></td>

                                    }
                                    {
                                        data.changButton == true ?
                                            <td>
                                                <input type="submit" class="btn btn-danger" onClick={() => props.changbutton(index)} />
                                            </td>
                                            :
                                            <td>
                                                <input type="submit" class="btn btn-secondary" value="Done" onClick={() => props.chang(index)} />

                                            </td>
                                    }
                                </tr>
                            )
                        }
                        )
                        : <h1>NoData</h1>
                }

            </tbody>
        </table >

    )
}


export default Display;























